#!/bin/bash
# postinst script for xivocc-installer
#
# see: dh_installdeb(1)

set -e

COMPOSE_PATH="/etc/docker/compose"
COMPOSE_FILE="docker-xivocc.yml"
ENV_FILE=".env"

XUC_SCRIPT="/usr/bin/configure-xivo"
XUC_PORT=8090

XIVO_SOLUTIONS_DEV="xivo-solutions-dev"
XIVO_SOLUTIONS_RC="xivo-solutions-rc"
XIVO_SOLUTIONS_PROD="xivo-solutions"
LATESTDEV="00-latestdev"
LATESTRC="latestrc"
LATEST="latest"
DEFAULT_VERSION="2017.02"

RESTART_REPLY=false
CONFIGURE_REPLY=false

get_distribution() {
    distribution=$(apt-cache showpkg xivocc-installer | grep -A 1 Versions: | sed 's/.*dists_//' | sed 's/_main.*//' | sed '/Versions:/d')
    if [ "$distribution" == "$XIVO_SOLUTIONS_PROD" ]; then
        distribution="$LATEST"
    elif [ "$distribution" == "$XIVO_SOLUTIONS_RC" ]; then
        distribution="$LATESTRC"
    elif [ "$distribution" == "$XIVO_SOLUTIONS_DEV" ]; then
        distribution="$LATESTDEV"
    else
        distribution="$LATEST"
    fi
    echo ${distribution}
}

get_tag() {
    tag=${DEFAULT_VERSION}
}

generate_env_file() {
    echo -e "\e[1;33mGenerating docker ${ENV_FILE} file\e[0m"
    echo "XIVOCC_TAG=${1}" > ${COMPOSE_PATH}/${ENV_FILE}
    echo "XIVOCC_DIST=${2}" >> ${COMPOSE_PATH}/${ENV_FILE}
    echo "XIVO_HOST=${XIVO_HOST}" >> ${COMPOSE_PATH}/${ENV_FILE}
    echo "XUC_HOST=${XUC_HOST}" >> ${COMPOSE_PATH}/${ENV_FILE}
    echo "XUC_PORT=${XUC_PORT}" >> ${COMPOSE_PATH}/${ENV_FILE}
    echo "WEEKS_TO_KEEP=${WEEKS_TO_KEEP}" >> ${COMPOSE_PATH}/${ENV_FILE}
    echo "RECORDING_WEEKS_TO_KEEP=${RECORDING_WEEKS_TO_KEEP}" >> ${COMPOSE_PATH}/${ENV_FILE}
    echo "XIVO_AMI_SECRET=${3}" >> ${COMPOSE_PATH}/${ENV_FILE}

}

get_xivo_host() {
    if [ -f ${COMPOSE_PATH}/${ENV_FILE} ]; then
        XIVO_HOST=$(grep -oP -m 1 'XIVO_HOST=\K.*' ${COMPOSE_PATH}/${ENV_FILE})
    else
        whiptail --title "Xivo IP address Not found" --msgbox "Please enter the XiVO PBX IP address." 8 50
        XIVO_HOST=$(whiptail --inputbox "Enter the XiVO PBX IP address: " 8 50 3>&1 1>&2 2>&3)
    fi
}

get_xuc_host() {
    if [ -f ${COMPOSE_PATH}/${ENV_FILE} ]; then
        XUC_HOST=$(grep -oP -m 1 'XUC_HOST=\K.*' ${COMPOSE_PATH}/${ENV_FILE})
    else
        whiptail --title "XUC IP address Not found" --msgbox "Please enter this machine's external IP address." 8 50
        XUC_HOST=$(whiptail --inputbox "Enter this machine's external IP address: " 8 50 3>&1 1>&2 2>&3)
    fi
}

get_weeks() {
    if [ -f ${COMPOSE_PATH}/${ENV_FILE} ]; then
        WEEKS_TO_KEEP=$(grep -oP -m 1 'WEEKS_TO_KEEP=\K.*' ${COMPOSE_PATH}/${ENV_FILE})
    else
        whiptail --title "Week to keep statistics not found" --msgbox "Please enter the number of weeks to keep for the statistics." 8 50
        WEEKS_TO_KEEP=$(whiptail --inputbox "Enter the number of weeks to keep for the statistics. " 8 50 3>&1 1>&2 2>&3)
    fi
}

get_recording_weeks() {
    if [ -f ${COMPOSE_PATH}/${ENV_FILE} ]; then
        RECORDING_WEEKS_TO_KEEP=$(grep -oP -m 1 'RECORDING_WEEKS_TO_KEEP=\K.*' ${COMPOSE_PATH}/${ENV_FILE})
    else
        whiptail --title "Recording weeks to keep not found" --msgbox "Please enter the number of weeks to keep for the recording files." 8 50
        RECORDING_WEEKS_TO_KEEP=$(whiptail --inputbox "Enter the number of weeks to keep for the recording files." 8 50 3>&1 1>&2 2>&3)
    fi
}


get_ami_secret() {
    if [ -f ${COMPOSE_PATH}/${ENV_FILE} ]; then
        XIVO_AMI_SECRET=$(grep -oP -m 1 'XIVO_AMI_SECRET=\K.*' ${COMPOSE_PATH}/${ENV_FILE})
    else
        whiptail --title "AMI secret password not found" --msgbox "Please enter your AMI secret." 8 50
        XIVO_AMI_SECRET=$(whiptail --inputbox "Please enter your AMI secret." 8 50 3>&1 1>&2 2>&3)
    fi
}

create_log_rotate() {
    echo "
    /var/lib/docker/containers/*/*.log {
      rotate 7
      daily
      compress
      missingok
      delaycompress
      copytruncate
    }
    " > /etc/logrotate.d/docker-container
}

create_nginx_certificate() {
    SSLDIR=/etc/docker/nginx/ssl
    mkdir -p $SSLDIR
    openssl req -nodes -newkey rsa:2048 -keyout $SSLDIR/xivoxc.key -out $SSLDIR/xivoxc.csr -subj "/C=FR/ST=/L=/O=Avencall/OU=/CN=$(hostname --fqdn)"
    openssl x509 -req -days 3650 -in $SSLDIR/xivoxc.csr -signkey $SSLDIR/xivoxc.key -out $SSLDIR/xivoxc.crt
}

create_dcomp_alias() {
    sed -e '/alias dcomp=/ s/^#*/#/' -i ~/.bashrc
    echo "alias dcomp='function _dcomp() { ( cd ${COMPOSE_PATH} && docker-compose -p xivocc -f ${COMPOSE_PATH}/${COMPOSE_FILE} \$@; ) }; _dcomp'" >> ~/.bashrc
}

create_ami_secret() {
    echo $(< /dev/urandom tr -dc A-Za-z0-9 | head -c${1:-11};)
}

check_if_xivo_reachable() {
    if ping -c 1 $XIVO_HOST &> /dev/null
    then
      return 0
    else
      return 1
    fi
}

ask_restart_xivo() {
    if (whiptail --title "Restart XiVO PBX" --yesno "XiVO PBX will be reconfigured during the installation and must be restarted. Do you want to restart it during the installation?" 8 78); then
        RESTART_REPLY=true;
    else
        RESTART_REPLY=false ;
    fi
}

ask_configure_xivo() {
    if (whiptail --title "Configure XiVO PBX with XUC setup" --yesno "Would you like to configure your XiVO PBX? Consider your already present XUC configuration please." 8 78); then
        CONFIGURE_REPLY=true;
    else
        CONFIGURE_REPLY=false ;
    fi
}

ask_questions() {
    # General message
    whiptail --title "XUC - XiVO PBX Configuration Details" --msgbox "You will be asked XiVO PBX & XiVO CC configuration questions." 8 50
    # Ask XiVO Network Questions
    echo "Ask for XiVO PBX IP"
    XIVO_HOST=$(whiptail --inputbox "Enter the XiVO PBX IP address: " 8 50 3>&1 1>&2 2>&3)
    echo "Got ${XIVO_HOST}"
    echo "Ask for XiVO CC IP"
    XUC_HOST=$(whiptail --inputbox "Enter this machine's external IP address: " 8 50 3>&1 1>&2 2>&3)
    echo "Got ${XUC_HOST}"

    # Ask whether to configure XiVO PBX with XUC
    ask_configure_xivo

    # Ask when to restart XiVO PBX
    if [ "$CONFIGURE_REPLY" = true ]; then
        ask_restart_xivo
    fi

    # ASk XiVOCC Generic Questions
    WEEKS_TO_KEEP=$(whiptail --inputbox "Enter the number of weeks to keep for the statistics: " 8 50 3>&1 1>&2 2>&3)
    RECORDING_WEEKS_TO_KEEP=$(whiptail --inputbox "Enter the number of weeks to keep for the recording files: " 8 50 3>&1 1>&2 2>&3)
}
case "$1" in
    configure)
        if [ -z "$2" ]; then
            if ! hostname --fqdn; then
                echo -e "\e[1;31mFQDN not set correctly, please check hostname and /etc/hosts. Exiting...\e[0m"
                exit 1
            fi
            ask_questions
            echo -e "\e[1;33mXiVO PBX Configuration...\e[0m"
            echo -e "\e[1;33mGenerating keys, when asked for passphrase leave empty.\e[0m"
            ssh-keygen -t rsa -f ~/.ssh/xivocc_rsa
            if ! check_if_xivo_reachable ;
            then
                whiptail --title "XiVO unreachable" --msgbox "Please check your network setup and re-run the installation." 8 50
                exit 1
            else
                cat ~/.ssh/xivocc_rsa.pub | ssh root@$XIVO_HOST 'mkdir -p .ssh && cat >> .ssh/authorized_keys'
            fi

            # Get package version
            get_tag

            # Get package distribution
            distribution=$(get_distribution)

            if [ "$CONFIGURE_REPLY" = true ]; then
                # Create AMI secret
                xivo_ami_secret=$(create_ami_secret)

                # Working on remote XiVO PBX
                # Copy the xuc setup script to XiVO PBX
                scp -i ~/.ssh/xivocc_rsa ${XUC_SCRIPT} root@${XIVO_HOST}:/usr/bin

                # Run the xuc setup script on XiVO PBX
                ssh -i ~/.ssh/xivocc_rsa root@${XIVO_HOST} "cd /tmp && bash $XUC_SCRIPT $XUC_HOST $RESTART_REPLY $xivo_ami_secret"
            else
                echo -e "\e[1;32mXiVO PBX will not be configured\e[0m"
            fi

            # Add log rotate
            create_log_rotate

            echo -e "\e[1;33mXiVO CC Configuration...\e[0m"

            # Generic XiVO CC config
            mkdir -p ${COMPOSE_PATH}

            # Create env file for Docker
            generate_env_file ${tag} ${distribution} ${xivo_ami_secret}

            mkdir -p /var/log/xivocc
            chown -R daemon:daemon /var/log/xivocc

            # Recording config
            mkdir -p /var/spool/recording-server
            chown daemon:daemon /var/spool/recording-server
            chmod 760 /var/spool/recording-server

            # Self-signed certificate for NGINX
            create_nginx_certificate

            # Create dcomp alias
            create_dcomp_alias

            echo "XiVO CC is installed. To launch it, execute 'dcomp up -d'.
            If a pack reporting was previously installed with the old packaging system, please migrate the database first.
            If you are using XiVO CC with a version of XiVO less than 15.15, please modify ${COMPOSE_PATH}/${COMPOSE_FILE} to use the proper ast11 images."
            if [ "$CONFIGURE_REPLY" = false ]; then
                echo -e "\e[1;33mYou chose not to configure XiVO PBX\e[0m"
                echo -e "\e[1;33mPlease manually edit line XIVO_AMI_SECRET in ${COMPOSE_PATH}/${ENV_FILE}\e[0m"
            fi
            echo -e "\e[1;33mPlease run source ~/.bashrc \e[0m"
            echo -e "\e[1;33mAnd start by dcomp up -d\e[0m"
            echo -e "\e[1;33mThen open http://$XUC_HOST in your browser\e[0m"

        else
            # Extract XiVO PBX and XUC (this machine) ip address
            get_xivo_host
            get_xuc_host
            get_weeks
            get_recording_weeks
            get_ami_secret

            # Move .env file to archive file
            if [ -f ${COMPOSE_PATH}/${ENV_FILE} ]; then
                mv -f ${COMPOSE_PATH}/${ENV_FILE} ${COMPOSE_PATH}/${ENV_FILE}.dpkg-old
            fi

            # Get package version
            echo -e "\e[1;33mGetting package version...\e[0m"
            get_tag
            # Get package distribution
            echo -e "\e[1;33mGetting package distribution...\e[0m"
            distribution=$(get_distribution)
            generate_env_file ${tag} ${distribution} ${XIVO_AMI_SECRET}
            create_dcomp_alias
            echo -e "\e[1;33mPlease run source ~/.bashrc \e[0m"
            echo -e "\e[1;33mAnd start by dcomp up -d\e[0m"

        fi;
    ;;
    abort-upgrade|abort-remove|abort-deconfigure)
    ;;
    *)
        echo "postinst called with unknown argument '$1'" >&2
        exit 1
    ;;
esac
# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.
#DEBHELPER#
exit 0
